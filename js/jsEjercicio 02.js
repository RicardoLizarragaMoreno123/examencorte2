function celsiusToFahrenheit(celsius) {
    return (9 / 5) * celsius + 32;
}

function fahrenheitToCelsius(fahrenheit) {
    return (fahrenheit - 32) * 5/9;
}

function realizarConversion() {
    var temperatura = parseFloat(document.getElementById("temperatura").value);
    var conversion = document.getElementById("conversion").value;
    var resultado;

    if (conversion === "celsiusToFahrenheit") {
        resultado = celsiusToFahrenheit(temperatura).toFixed(2) + " grados Fahrenheit";
    } else if (conversion === "fahrenheitToCelsius") {
        resultado = fahrenheitToCelsius(temperatura).toFixed(2) + " grados Celsius";
    }

    document.getElementById("resultado").textContent = resultado;
}