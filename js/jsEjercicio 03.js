var edadesGeneradas = [];

        // Función para generar edades aleatorias
        function generarEdadesAleatorias() {
            edadesGeneradas = [];
            for (var i = 0; i < 100; i++) {
                var edad = Math.floor(Math.random() * 91); // Generar edades entre 0 y 90 años
                edadesGeneradas.push(edad);
            }
            mostrarEdadesYTotales();
        }

        // Función para clasificar las edades
        function clasificarEdades(edades) {
            var bebes = 0;
            var ninos = 0;
            var adolescentes = 0;
            var adultos = 0;
            var ancianos = 0;

            for (var i = 0; i < edades.length; i++) {
                var edad = edades[i];

                if (edad >= 1 && edad <= 3) {
                    bebes++;
                } else if (edad >= 4 && edad <= 12) {
                    ninos++;
                } else if (edad >= 13 && edad <= 17) {
                    adolescentes++;
                } else if (edad >= 18 && edad <= 60) {
                    adultos++;
                } else {
                    ancianos++;
                }
            }

            return {
                bebes,
                ninos,
                adolescentes,
                adultos,
                ancianos
            };
        }

        // Función para mostrar las edades y totales
        function mostrarEdadesYTotales() {
            var edadesParrafo = document.getElementById("edades");
            edadesParrafo.textContent = "Edades: " + edadesGeneradas.join(", ");
            
            var resultadoParrafo = document.getElementById("resultado");
            var clasificacion = clasificarEdades(edadesGeneradas);
            resultadoParrafo.textContent = "Bebés: " + clasificacion.bebes + ", Niños: " + clasificacion.ninos + ", Adolescentes: " + clasificacion.adolescentes + ", Adultos: " + clasificacion.adultos + ", Ancianos: " + clasificacion.ancianos;
        }

        // Función para limpiar los números generados y los totales
        function limpiar() {
            edadesGeneradas = [];
            mostrarEdadesYTotales();
        }

        // Mostrar edades y totales al cargar la página
        mostrarEdadesYTotales();