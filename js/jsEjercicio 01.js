function calcularCostoBoleto() {
    var numBoleto = document.getElementById("numBoleto").value;
    var nombreCliente = document.getElementById("nombreCliente").value;
    var destino = document.getElementById("destino").value;
    var tipoViaje = parseInt(document.querySelector('input[name="tipoViaje"]:checked').value);

    // Definir las tarifas base para cada destino
    var tarifas = {
        "Ciudad A": 100,
        "Ciudad B": 120,
        "Ciudad C": 90
    };

    var tarifa_base = tarifas[destino];

    // Aplicar incremento del 80% para viajes dobles
    if (tipoViaje === 2) {
        tarifa_base *= 1.8;
    }

    // Calcular el costo total del boleto
    var costo_total = tarifa_base;

    // Calcular el impuesto del 16%
    var impuesto = costo_total * 0.16;

    // Sumar el impuesto al costo total
    costo_total += impuesto;

    document.getElementById("costoBoleto").textContent = "$" + costo_total.toFixed(2);
    document.getElementById("impuesto").textContent = "$" + impuesto.toFixed(2);
}

